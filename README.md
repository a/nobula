# nobula

A tool to download videos from watchnebula.com. Requires a subscription, obviously.

If it gets stuck during one of the download phases, blame Zype. Their CDN isn't the best.

## Screenshot

![Screenshot of the tool running](https://elixi.re/i/t0ocwtbi.png)

## Setup

- install geckodriver from OS package manager or upstream https://github.com/mozilla/geckodriver
- Clone or download the repo
- Install python3.6 or higher if you don't have it installed already
- Install ffmpeg if you don't have it installed already
- Install the requirements on `requirements.txt` with `pip3 install -Ur requirements.txt --user`
- Copy `config.template.json` to `config.json`, fill in either `email` and `password` or `token`. If you only fill in `email` and `password`, `token` will be automatically filled when you first run the script.

## Usage

After going through setup, run it as `nobula.py video-url output-filename`.

Example: `python3.7 nobula.py https://watchnebula.com/videos/hbomberguy-climate-denial-a-measured-response measured-response.mp4`.

The output filename was only tested with mp4s.

## Personal note to Nebula

Thank you for not having DRM. That's a genuinely good thing.

Try not to rely on Zype so much. I'd love to see an open source solution made by you, or at least something like peertube with subscription support (and... uh... no federation IG?).

Please don't use things like Google Analytics. I don't want to be spied on by Google, while trying to support my favorite creators create a new platform that isn't by Google.

Your service seems to be really barebones rn, there's no follower count, no comments, and most people only have recent videos (T1J only has his most recent 25 or so videos), and some don't even have their latest ones (hbomb doesn't have his scanline video for example). I think the system would be much better if it had these. Even using reddit for comments would be better than nothing I think.

Also I got random 500s here and there, during login and logout. Waiting a bit fixed it and as a dev I don't care much, but from a UX perspective, it's not very good.

## Disclaimer

This project is not affiliated with Nebula, it is also not endorsed by them.

Please do not use this software for illegal purposes like piracy, but rather for legal uses like personal backups (if it is legal in your jurisdiction-- it is in mine).

Usage of this tool might be a violation of Nebula ToS, that's your responsibility.

I don't accept any responsibility whatsoever.
