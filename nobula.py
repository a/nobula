import os
import sys
import json
import m3u8
import shutil
import logging
import requests
import subprocess
#from bs4 import BeautifulSoup

from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium import webdriver
from urllib.parse import urlparse, parse_qs

def download_file(url, filename):
    # Based on http://masnun.com/2016/09/18/
    # python-using-the-requests-module-to-download-large-files-efficiently.html
    resp = requests.get(url, stream=True)
    if resp.status_code != 200:
        return False
    handle = open(filename, "wb")
    for chunk in resp.iter_content(chunk_size=512):
        if chunk:  # filter out keep-alive new chunks
            handle.write(chunk)
    return True


def get_master_m3u8(url, token):
    cookies = {
        "nebula-auth": "{%22apiToken%22:%22"
        + token
        + "%22%2C%22isLoggingIn%22:false%2C%22isLoggingOut%22:false}"
    }

    options = FirefoxOptions()
    options.add_argument("--headless")
    driver = webdriver.Firefox(options=options)

    driver.get(url)
    for name, value in cookies.items():
        driver.add_cookie({'name':name, 'value':value})

    driver.get(url)
    iframe = driver.find_element_by_xpath("//iframe[starts-with(@class,'VideoPlayer')]")
    vid_id = iframe.get_attribute('id').split('_')[-1]
    src_url = iframe.get_attribute('src')
    zype_token = parse_qs( urlparse(src_url).query )['access_token'][0]

    #out = requests.get(url, cookies=cookies).text
    #print (out)
    #bsd = BeautifulSoup(out, features="lxml")
    #print (bsd.find_all('a'))
    #script = bsd.find(id="initial-app-state").contents[0]
    #scriptj = json.loads(script)
    #zype_token = scriptj["account"]["userInfo"]["zypeAuthInfo"]["accessToken"]
    #vid_id = list(scriptj["videos"]["byID"].keys())[0]

    zype_url = f"https://player.zype.com/manifest/{vid_id}.m3u8?access_token={zype_token}&ad_enabled=false&https=true&preview=false"
    zype_master_m3u8 = m3u8.load(zype_url)
    return zype_master_m3u8

def get_hq_m3u8(master_m3u8):
    best_quality_url = master_m3u8.playlists[-2].uri
    return m3u8.load(best_quality_url)


def do_login(email, password, token=""):
    """Does login with given email and password, returns auth token."""
    if token:
        return token

    resp = requests.post(
        f"https://api.watchnebula.com/api/v1/auth/login/",
        json={"email": email, "password": password}
    )
    if resp.status_code != 200:
        logging.error(resp.text)
        sys.exit(2)
    respj = resp.json()
    return respj["key"]


if __name__ == "__main__":
    print("nobula, a tool to download videos from nebula")
    print("GPLv3 | https://gitlab.com/a/nobula")
    print(
        "Please do not use this software for illegal purposes like piracy, but rather for legal uses like personal backups (if it is legal in your jurisdiction-- it is in mine)."
    )
    logging.basicConfig(level=logging.INFO)

    # TODO: use something decent for args
    if len(sys.argv) < 3:
        logging.error("No video URL or output filename supplied.")

    url = sys.argv[1]
    out_filename = sys.argv[2]

    # Account for no config
    if not os.path.exists("config.json"):
        logging.error(
            "No config found, please create one based on config.template.json."
        )
        sys.exit(1)

    # Read config
    with open("config.json", "r") as f:
        config = json.load(f)

    # Do auth, save token if we used email/password
    auth_token = do_login(config["email"], config["password"], config["token"])
    if config["token"] != auth_token:
        config["token"] = auth_token

        with open("config.json", "w") as f:
            json.dump(config, f)
        logging.info("Login successful, saved token, will use that from now on.")

    # Do the actual m3u8 fetching
    logging.info("Getting master m3u8")
    master_m3u8 = get_master_m3u8(url, auth_token)
    logging.info("Got master m3u8, getting highest quality m3u8 now.")

    hq_m3u8 = get_hq_m3u8(master_m3u8)
    logging.info("Got it. Working on downloading segments now.")

    # Create the temp folder
    os.makedirs("nobula_temp", exist_ok=True)

    counter = 0
    ffmpeg_text = ""
    for segment in hq_m3u8.segments:
        counter += 1
        logging.info(f"Downloading {counter}/{len(hq_m3u8.segments)}")
        filename = f"{counter}.ts"
        filepath = os.path.join("nobula_temp", filename)
        download_file(segment.uri, filepath)
        ffmpeg_text += f"file {filename}\n"

    logging.info(f"Done downloading, combining to {out_filename}.")

    filelist_filename = os.path.join("nobula_temp", "filelist.txt")
    with open(filelist_filename, "w") as f:
        f.write(ffmpeg_text)

    # Combine the files with ffmpeg
    subprocess.run(f"ffmpeg -f concat -i {filelist_filename} -c copy -bsf:a aac_adtstoasc {out_filename}", shell=True, check=True)
    logging.info(f"Combined into {filename}, deleting temp folder.")

    # Delete the temp folder
    shutil.rmtree("nobula_temp")
    logging.info(f"Deleted temp folder, all done, bye!")
